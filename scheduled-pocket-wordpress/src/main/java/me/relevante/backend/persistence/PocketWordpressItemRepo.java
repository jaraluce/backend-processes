package me.relevante.backend.persistence;

import me.relevante.backend.model.PocketWordpressItem;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PocketWordpressItemRepo extends MongoRepository<PocketWordpressItem, String> {
    PocketWordpressItem findOneByPocketItemIdAndWordpressHubId(String pocketItemId, String wordpressHubId);
}
