package me.relevante.backend.persistence;

import me.relevante.request.TwitterExtractProfileActivityRequest;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/**
 * @author Daniel Ibanez
 */
@Repository
public interface TwitterExtractProfileActivityRequestRepo extends MongoRepository<TwitterExtractProfileActivityRequest, String> {
}
