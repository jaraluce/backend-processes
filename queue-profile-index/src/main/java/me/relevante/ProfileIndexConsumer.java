
package me.relevante;

import me.relevante.backend.Consumer;
import me.relevante.backend.queue.QueueName;
import me.relevante.queue.ProfileIndexMessage;
import me.relevante.service.ProfileIndexService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ProfileIndexConsumer implements Consumer<ProfileIndexMessage> {

    private static final Logger logger = LoggerFactory.getLogger(ProfileIndexConsumer.class);

    private final ProfileIndexService profileIndexService;

    @Autowired
    public ProfileIndexConsumer(final ProfileIndexService profileIndexService) {
        this.profileIndexService = profileIndexService;
    }

    @RabbitListener(queues = QueueName.PROFILE_INDEX)
	public void process(final ProfileIndexMessage request) {
        try {
            profileIndexService.process(request);
        } catch (Exception e) {
            logger.error("Error processing Twitter bulk action", e);
        }
    }

}