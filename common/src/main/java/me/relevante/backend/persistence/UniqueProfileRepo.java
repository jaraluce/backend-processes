package me.relevante.backend.persistence;

import me.relevante.core.UniqueProfile;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/**
 * @author Daniel Ibanez
 */
@Repository
public interface UniqueProfileRepo extends MongoRepository<UniqueProfile, String> {
}
