package me.relevante.service.impl;

import me.relevante.service.NetworkLastPostAssigner;
import me.relevante.core.Network;
import me.relevante.core.NetworkFullPost;
import me.relevante.core.NetworkFullProfileWithPosts;
import me.relevante.core.RelevanteProfileContext;
import me.relevante.persistence.NetworkFullPostRepo;
import me.relevante.request.NetworkEngageStatus;
import me.relevante.service.RelevanteProfileContextService;
import org.springframework.data.repository.CrudRepository;

import java.util.Date;
import java.util.List;

public abstract class AbstractLastPostAssigner<N extends Network, P extends NetworkFullPost<N, ?, P>, F extends NetworkFullProfileWithPosts<N, ?, P, F>, E extends NetworkEngageStatus<N, E>>
        implements NetworkLastPostAssigner<N> {

    private final CrudRepository<F, String> fullProfileRepo;
    private final NetworkFullPostRepo<P> fullPostRepo;
    private final RelevanteProfileContextService relevanteProfileContextService;

    public AbstractLastPostAssigner(final CrudRepository<F, String> fullProfileRepo,
                                    final NetworkFullPostRepo<P> fullPostRepo,
                                    final RelevanteProfileContextService relevanteProfileContextService) {
        this.fullProfileRepo = fullProfileRepo;
        this.fullPostRepo = fullPostRepo;
        this.relevanteProfileContextService = relevanteProfileContextService;
    }

    @Override
    public void assignLastPost(final String networkProfileId,
                               final String relevanteId) {
        final F fullProfile = fullProfileRepo.findOne(networkProfileId);
        final List<P> fullPosts = fullPostRepo.findByPostAuthorId(fullProfile.getId());
        if (fullPosts.isEmpty()) {
            return;
        }
        fullPosts.sort((o1, o2) -> {
            final Date creationDate1 = o1.getPost().getCreationTimestamp();
            final Date creationDate2 = o2.getPost().getCreationTimestamp();
            if (creationDate2 == null && creationDate1 == null) return 0;
            if (creationDate1 == null) return -1;
            if (creationDate2 == null) return 1;
            return creationDate2.compareTo(creationDate1);
        });
        final P lastPost = fullPosts.get(0);
        if (fullProfile.getLastPost() != null && fullProfile.getLastPost().getId().equals(lastPost.getId())) {
            return;
        }
        fullProfile.setLastPost(lastPost);
        fullProfileRepo.save(fullProfile);
        final RelevanteProfileContext relevanteProfileContext = relevanteProfileContextService.getOrCreate(relevanteId,
                fullProfile.getUniqueProfileId());
        E engageStatus = relevanteProfileContext.getEngageStatus(getNetwork().getName());
        if (engageStatus == null) {
            engageStatus = createNetworkEngageStatus();
            relevanteProfileContext.setEngageStatus(engageStatus);
        }
        engageStatus.setLevel1Status(null);
        engageStatus.setLevel2Status(null);
        relevanteProfileContextService.save(relevanteProfileContext);
    }

    protected abstract E createNetworkEngageStatus();
}
