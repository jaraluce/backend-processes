package me.relevante.backend.persistence;

import me.relevante.persistence.BaseSearchProfileRepo;
import me.relevante.search.SearchProfile;
import org.springframework.stereotype.Repository;

/**
 * @author daniel-ibanez
 */
@Repository
public interface SearchProfileRepo extends BaseSearchProfileRepo {
    SearchProfile findOneById(String id);
}
