package me.relevante.backend.persistence;

import me.relevante.core.TwitterFav;
import me.relevante.persistence.NetworkPostActionRepo;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TwitterFavRepo extends NetworkPostActionRepo<TwitterFav>, MongoRepository<TwitterFav, String> {
}
