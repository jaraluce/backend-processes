package me.relevante.backend;

import me.relevante.queue.QueueMessage;

public interface Consumer<T extends QueueMessage> {
    void process(final T request);
}