package me.relevante.service.impl;

import me.relevante.service.NetworkLastPostAssigner;
import me.relevante.core.Linkedin;
import me.relevante.core.LinkedinFullPost;
import me.relevante.core.LinkedinFullProfile;
import me.relevante.persistence.NetworkFullPostRepo;
import me.relevante.request.LinkedinEngageStatus;
import me.relevante.service.RelevanteProfileContextService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Component;

@Component
public class LinkedinLastPostAssigner
        extends AbstractLastPostAssigner<Linkedin, LinkedinFullPost, LinkedinFullProfile, LinkedinEngageStatus>
        implements NetworkLastPostAssigner<Linkedin> {

    @Autowired
    public LinkedinLastPostAssigner(final CrudRepository<LinkedinFullProfile, String> fullProfileRepo,
                                    final NetworkFullPostRepo<LinkedinFullPost> fullPostRepo,
                                    final RelevanteProfileContextService relevanteProfileContextService) {
        super(fullProfileRepo, fullPostRepo, relevanteProfileContextService);
    }

    @Override
    public Linkedin getNetwork() {
        return Linkedin.getInstance();
    }

    @Override
    protected LinkedinEngageStatus createNetworkEngageStatus() {
        return new LinkedinEngageStatus();
    }

}
