package me.relevante.service.impl;

import me.relevante.api.TwitterApiClient;
import me.relevante.api.TwitterApiException;
import me.relevante.api.TwitterApiResponse;
import me.relevante.api.TwitterApiResult;
import me.relevante.service.NetworkBulkRequestService;
import me.relevante.core.RelevanteAccount;
import me.relevante.core.RelevanteContext;
import me.relevante.core.Twitter;
import me.relevante.core.TwitterFullProfile;
import me.relevante.core.TwitterReply;
import me.relevante.core.UniqueProfile;
import me.relevante.request.ActionEngageStatus;
import me.relevante.request.TwitterEngageStatus;
import me.relevante.request.TwitterReplyRequest;
import me.relevante.service.RelevanteProfileContextService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

@Service
public class TwitterReplyRequestService
        extends AbstractTwitterEngageBulkRequestService<TwitterReplyRequest, TwitterReply>
        implements NetworkBulkRequestService<Twitter, TwitterReplyRequest> {

    @Autowired
    public TwitterReplyRequestService(final CrudRepository<TwitterReplyRequest, String> replyRequestRepo,
                                      final CrudRepository<RelevanteContext, String> relevanteContextRepo,
                                      final CrudRepository<TwitterReply, String> replyRepo,
                                      final CrudRepository<UniqueProfile, String> uniqueProfileRepo,
                                      final CrudRepository<TwitterFullProfile, String> fullProfileRepo,
                                      final RelevanteProfileContextService relevanteProfileContextService,
                                      final CrudRepository<RelevanteAccount, String> relevanteAccountRepo) {
        super(replyRequestRepo, relevanteContextRepo, replyRepo, uniqueProfileRepo, fullProfileRepo, relevanteProfileContextService,
                relevanteAccountRepo);
    }

    @Override
    public Class<TwitterReplyRequest> getActionRequestClass() {
        return TwitterReplyRequest.class;
    }

    @Override
    protected TwitterReply createActionFromActionRequest(final TwitterReplyRequest replyRequest) {
        return new TwitterReply(replyRequest.getRequesterRelevanteId(), replyRequest.getTargetProfileId(), replyRequest.getTargetPostId(),
                replyRequest.getReplyText());
    }

    @Override
    protected TwitterApiResponse executeApiAction(final TwitterReplyRequest actionRequest, final TwitterApiClient apiClient) {
        final TwitterFullProfile fullProfile = fullProfileRepo.findOne(actionRequest.getTargetProfileId());
        try {
            return apiClient.replyPost(actionRequest.getTargetPostId(), fullProfile.getProfile().getScreenName(), actionRequest.getReplyText());
        } catch (final TwitterApiException e) {
            return new TwitterApiResponse(TwitterApiResult.FORBIDDEN, e);
        }
    }

    @Override
    protected void updateActionEngageStatus(final TwitterEngageStatus engageStatus, final ActionEngageStatus actionEngageStatus) {
        engageStatus.setReplyStatus(actionEngageStatus);
    }
}
