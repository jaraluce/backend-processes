package me.relevante.backend.queue;

public final class QueueName {
    public static final String BULK_ACTIONS = "BulkActions";
    public static final String DEDUPLICATION = "Deduplication";
    public static final String SEND_EMAIL = "SendEmail";
    public static final String ENRICHMENT = "Enrichment";
    public static final String PROFILE_INDEX = "ProfileIndex";
    public static final String REMOVE_PROFILE_FROM_INDEX = "RemoveProfileFromIndex";
}