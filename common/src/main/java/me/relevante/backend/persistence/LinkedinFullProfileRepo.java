package me.relevante.backend.persistence;

import me.relevante.persistence.BaseLinkedinFullProfileRepo;
import org.springframework.stereotype.Repository;

@Repository
public interface LinkedinFullProfileRepo extends BaseLinkedinFullProfileRepo {
}