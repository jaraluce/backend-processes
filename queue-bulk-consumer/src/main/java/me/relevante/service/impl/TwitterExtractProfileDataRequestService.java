package me.relevante.service.impl;

import me.relevante.api.TwitterApiClient;
import me.relevante.api.TwitterApiException;
import me.relevante.api.TwitterApiResponse;
import me.relevante.api.TwitterApiResult;
import me.relevante.core.RelevanteAccount;
import me.relevante.core.RelevanteContext;
import me.relevante.core.Twitter;
import me.relevante.core.TwitterFullProfile;
import me.relevante.core.TwitterProfile;
import me.relevante.core.UniqueProfile;
import me.relevante.ingest.TwitterIngestProfile;
import me.relevante.persistence.BaseTwitterFullProfileRepo;
import me.relevante.request.TwitterExtractProfileDataRequest;
import me.relevante.service.NetworkBulkRequestService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

@Service
public class TwitterExtractProfileDataRequestService
        extends AbstractTwitterApiBulkRequestService<TwitterExtractProfileDataRequest>
        implements NetworkBulkRequestService<Twitter, TwitterExtractProfileDataRequest> {

    private static final Logger LOGGER = LoggerFactory.getLogger(TwitterExtractProfileDataRequestService.class);

    private final TwitterIngestProcessor ingestProcessor;
    private final TwitterIngestConverter ingestConverter;
    private final BaseTwitterFullProfileRepo fullProfileRepo;
    private final TwitterLastPostAssigner lastPostAssigner;

    @Autowired
    public TwitterExtractProfileDataRequestService(final CrudRepository<RelevanteContext, String> relevanteContextRepo,
                                                   final CrudRepository<TwitterExtractProfileDataRequest, String> extractProfileDataRequestRepo,
                                                   final CrudRepository<RelevanteAccount, String> relevanteAccountRepo,
                                                   final TwitterIngestProcessor ingestProcessor,
                                                   final TwitterIngestConverter ingestConverter,
                                                   final BaseTwitterFullProfileRepo fullProfileRepo,
                                                   final TwitterLastPostAssigner lastPostAssigner) {
        super(extractProfileDataRequestRepo, relevanteContextRepo, relevanteAccountRepo);
        this.ingestProcessor = ingestProcessor;
        this.ingestConverter = ingestConverter;
        this.fullProfileRepo = fullProfileRepo;
        this.lastPostAssigner = lastPostAssigner;
    }

    @Override
    public Class<TwitterExtractProfileDataRequest> getActionRequestClass() {
        return TwitterExtractProfileDataRequest.class;
    }

    @Override
    protected void execute(final TwitterExtractProfileDataRequest actionRequest) {
        super.execute(actionRequest);
        final UniqueProfile uniqueProfile = ingestProcessor.processIngestProfile(actionRequest.getExtractedProfile());
        lastPostAssigner.assignLastPost(uniqueProfile.getProfileIdByNetwork(getNetwork()), actionRequest.getRequesterRelevanteId());
    }

    @Override
    protected TwitterApiResponse executeApiAction(final TwitterExtractProfileDataRequest extractProfileDataRequest,
                                                  final TwitterApiClient apiClient) {
        TwitterApiResponse twitterApiResponse;
        try {
            final TwitterIngestProfile extractedProfile = extractProfileData(extractProfileDataRequest, apiClient);
            extractProfileDataRequest.setExtractedProfile(extractedProfile);
            extractProfileDataRequest.setSuccess();
            actionRequestRepo.save(extractProfileDataRequest);
            twitterApiResponse = new TwitterApiResponse(TwitterApiResult.SUCCESS, extractedProfile);
        } catch (final TwitterApiException e) {
            LOGGER.error("Error", e);
            extractProfileDataRequest.setError();
            twitterApiResponse = new TwitterApiResponse(TwitterApiResult.UNDEFINED_ERROR, e);
        }
        return twitterApiResponse;
    }

    private TwitterIngestProfile extractProfileData(final TwitterExtractProfileDataRequest extractProfileDataRequest,
                                                    final TwitterApiClient apiClient) {
        final TwitterFullProfile fullProfile = fullProfileRepo.findOne(extractProfileDataRequest.getTargetProfileId());
        final TwitterProfile profileFromApi = apiClient.getProfile(fullProfile.getProfile().getId());
        return ingestConverter.createIngestProfileFromProfile(profileFromApi);
    }
}
