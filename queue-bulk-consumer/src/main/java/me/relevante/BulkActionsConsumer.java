
package me.relevante;

import me.relevante.backend.Consumer;
import me.relevante.backend.queue.QueueName;
import me.relevante.service.BulkProcessService;
import me.relevante.queue.BulkActionMessage;
import me.relevante.queue.QueueService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class BulkActionsConsumer implements Consumer<BulkActionMessage> {

    private static final Logger logger = LoggerFactory.getLogger(BulkActionsConsumer.class);

    private final BulkProcessService bulkProcessService;
    private final QueueService queueService;

    @Autowired
    public BulkActionsConsumer(final BulkProcessService bulkProcessService,
                               final QueueService queueService) {
        this.bulkProcessService = bulkProcessService;
        this.queueService = queueService;
    }

    @RabbitListener(queues = QueueName.BULK_ACTIONS)
    public void process(final BulkActionMessage request) {
        try {
            final BulkActionMessage generatedRequest = bulkProcessService.process(request);
            if (generatedRequest != null) {
                queueService.sendBulkActionMessage(generatedRequest);
            }
        }
        catch (final Exception e) {
            logger.error("Error processing Twitter bulk action", e);
        }
    }

}