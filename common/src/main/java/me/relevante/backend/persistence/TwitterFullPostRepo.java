package me.relevante.backend.persistence;

import me.relevante.core.TwitterFullPost;
import me.relevante.persistence.NetworkFindByIdsRepo;
import me.relevante.persistence.NetworkFullPostRepo;
import org.springframework.stereotype.Repository;

@Repository
public interface TwitterFullPostRepo extends NetworkFullPostRepo<TwitterFullPost>, NetworkFindByIdsRepo<TwitterFullPost> {
    TwitterFullPost findOneByPostAuthorId(String authorId);
    TwitterFullPost findOneByPostId(String postId);
}
