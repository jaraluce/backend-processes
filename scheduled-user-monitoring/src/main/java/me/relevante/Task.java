package me.relevante;

import me.relevante.service.ProfileMonitoringService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class Task {

    @Autowired
    private ProfileMonitoringService profileMonitoringService;

    @Scheduled(fixedRateString = "${custom.schedule.monitoringService.activationScheduleInMilliseconds}")
    public void reportCurrentTime() {
        profileMonitoringService.monitor();
    }

}