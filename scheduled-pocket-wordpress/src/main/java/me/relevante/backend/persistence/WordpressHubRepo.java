package me.relevante.backend.persistence;

import me.relevante.core.WordpressHub;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by daniel-ibanez on 26/08/16.
 */
@Repository
public interface WordpressHubRepo extends MongoRepository<WordpressHub, String> {
    WordpressHub findByUrl(String url);
}
