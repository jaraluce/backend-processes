package me.relevante.backend.persistence;

import me.relevante.core.TwitterDirectMessage;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TwitterDirectMessageRepo extends MongoRepository<TwitterDirectMessage, String> {
	TwitterDirectMessage findOneByTargetProfileIdAndAuthorId(String targetProfileId, String authorId);
}
