package me.relevante.service.impl;

import me.relevante.service.NetworkBulkRequestService;
import me.relevante.core.Linkedin;
import me.relevante.core.LinkedinFullProfile;
import me.relevante.core.NetworkEngageAction;
import me.relevante.core.RelevanteContext;
import me.relevante.core.UniqueProfile;
import me.relevante.request.LinkedinEngageStatus;
import me.relevante.request.NetworkEngageActionRequest;
import me.relevante.service.RelevanteProfileContextService;
import org.springframework.data.repository.CrudRepository;

public abstract class AbstractLinkedinEngageBulkRequestService<R extends NetworkEngageActionRequest<Linkedin>, A extends NetworkEngageAction<Linkedin>>
        extends AbstractNetworkEngageBulkRequestService<Linkedin, R, A, LinkedinFullProfile, LinkedinEngageStatus>
        implements NetworkBulkRequestService<Linkedin, R> {

    public AbstractLinkedinEngageBulkRequestService(final CrudRepository<R, String> actionRequestRepo,
                                                    final CrudRepository<RelevanteContext, String> relevanteContextRepo,
                                                    final CrudRepository<A, String> actionRepo,
                                                    final CrudRepository<UniqueProfile, String> uniqueProfileRepo,
                                                    final CrudRepository<LinkedinFullProfile, String> fullProfileRepo,
                                                    final RelevanteProfileContextService relevanteProfileContextService) {
        super(actionRequestRepo, relevanteContextRepo, actionRepo, uniqueProfileRepo, fullProfileRepo, relevanteProfileContextService);
    }

    @Override
    public Linkedin getNetwork() {
        return Linkedin.getInstance();
    }

    @Override
    protected LinkedinEngageStatus createEngageStatusForCurrentNetwork() {
        return new LinkedinEngageStatus();
    }

    @Override
    protected void executeActionRequest(final R actionRequest) {
        // This work will be done by the extension
    }

}
