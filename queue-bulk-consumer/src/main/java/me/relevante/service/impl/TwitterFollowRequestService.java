package me.relevante.service.impl;

import me.relevante.api.TwitterApiClient;
import me.relevante.api.TwitterApiException;
import me.relevante.api.TwitterApiResponse;
import me.relevante.api.TwitterApiResult;
import me.relevante.service.NetworkBulkRequestService;
import me.relevante.core.RelevanteAccount;
import me.relevante.core.RelevanteContext;
import me.relevante.core.Twitter;
import me.relevante.core.TwitterFollow;
import me.relevante.core.TwitterFullProfile;
import me.relevante.core.UniqueProfile;
import me.relevante.request.ActionEngageStatus;
import me.relevante.request.TwitterEngageStatus;
import me.relevante.request.TwitterFollowRequest;
import me.relevante.service.RelevanteProfileContextService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

@Service
public class TwitterFollowRequestService
        extends AbstractTwitterEngageBulkRequestService<TwitterFollowRequest, TwitterFollow>
        implements NetworkBulkRequestService<Twitter, TwitterFollowRequest> {

    @Autowired
    public TwitterFollowRequestService(final CrudRepository<TwitterFollowRequest, String> followRequestRepo,
                                       final CrudRepository<RelevanteContext, String> relevanteContextRepo,
                                       final CrudRepository<TwitterFollow, String> followRepo,
                                       final CrudRepository<UniqueProfile, String> uniqueProfileRepo,
                                       final CrudRepository<TwitterFullProfile, String> fullProfileRepo,
                                       final RelevanteProfileContextService relevanteProfileContextService,
                                       final CrudRepository<RelevanteAccount, String> relevanteAccountRepo) {
        super(followRequestRepo, relevanteContextRepo, followRepo, uniqueProfileRepo, fullProfileRepo, relevanteProfileContextService,
                relevanteAccountRepo);
    }

    @Override
    public Class<TwitterFollowRequest> getActionRequestClass() {
        return TwitterFollowRequest.class;
    }

    @Override
    protected TwitterFollow createActionFromActionRequest(final TwitterFollowRequest followRequest) {
        return new TwitterFollow(followRequest.getRequesterRelevanteId(), followRequest.getTargetProfileId());
    }

    @Override
    protected TwitterApiResponse executeApiAction(final TwitterFollowRequest actionRequest, final TwitterApiClient apiClient) {
        try {
            return apiClient.follow(actionRequest.getTargetProfileId());
        } catch (final TwitterApiException e) {
            return new TwitterApiResponse(TwitterApiResult.FORBIDDEN, e);
        }
    }

    @Override
    protected void updateActionEngageStatus(final TwitterEngageStatus engageStatus, final ActionEngageStatus actionEngageStatus) {
        engageStatus.setFollowStatus(actionEngageStatus);
    }
}
