package me.relevante.service;

import me.relevante.backend.persistence.TwitterExtractProfileActivityRequestRepo;
import me.relevante.backend.persistence.TwitterExtractProfileDataRequestRepo;
import me.relevante.backend.persistence.TwitterSearchRelatedDataRequestRepo;
import me.relevante.core.RelevanteAccount;
import me.relevante.core.Twitter;
import me.relevante.core.TwitterFullProfile;
import me.relevante.queue.BulkActionMessage;
import me.relevante.queue.QueueService;
import me.relevante.request.TwitterExtractProfileActivityRequest;
import me.relevante.request.TwitterExtractProfileDataRequest;
import me.relevante.request.TwitterSearchRelatedDataRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author daniel-ibanez
 */
@Component
public class TwitterProfileMonitoringService
        extends AbstractNetworkProfileMonitoringService<Twitter, TwitterFullProfile>
        implements NetworkProfileMonitoringService<Twitter> {

    private final CrudRepository<TwitterFullProfile, String> fullProfileRepo;
    private final TwitterExtractProfileDataRequestRepo extractProfileDataRequestRepo;
    private final TwitterExtractProfileActivityRequestRepo extractProfileActivityRequestRepo;
    private final TwitterSearchRelatedDataRequestRepo searchRelatedDataRequestRepo;
    private final QueueService queueService;

    @Autowired
    public TwitterProfileMonitoringService(final CrudRepository<TwitterFullProfile, String> fullProfileRepo,
                                           final TwitterExtractProfileDataRequestRepo extractProfileDataRequestRepo,
                                           final TwitterExtractProfileActivityRequestRepo extractProfileActivityRequestRepo,
                                           final TwitterSearchRelatedDataRequestRepo searchRelatedDataRequestRepo,
                                           final QueueService queueService) {
        this.fullProfileRepo = fullProfileRepo;
        this.extractProfileDataRequestRepo = extractProfileDataRequestRepo;
        this.extractProfileActivityRequestRepo = extractProfileActivityRequestRepo;
        this.searchRelatedDataRequestRepo = searchRelatedDataRequestRepo;
        this.queueService = queueService;
    }

    @Override
    public Twitter getNetwork() {
        return Twitter.getInstance();
    }

    @Override
    public void processSearchTerms(final List<String> searchTerms,
                                   final RelevanteAccount relevanteAccount) {
        if (relevanteAccount.isNotConnectedToNetwork(getNetwork())) {
            return;
        }
        final TwitterSearchRelatedDataRequest searchRelatedDataRequest = new TwitterSearchRelatedDataRequest(relevanteAccount.getId(),
                searchTerms);
        final TwitterSearchRelatedDataRequest searchRelatedDataRequestWithId = searchRelatedDataRequestRepo.save(searchRelatedDataRequest);
        queueService.sendBulkActionMessage(new BulkActionMessage(searchRelatedDataRequestWithId));
    }

    @Override
    public void processProfile(final String networkProfileId,
                               final RelevanteAccount relevanteAccount) {
        if (relevanteAccount.isNotConnectedToNetwork(getNetwork())) {
            return;
        }
        final TwitterFullProfile fullProfile = fullProfileRepo.findOne(networkProfileId);
        if (isProfileActivityStale(fullProfile)) {
            final TwitterExtractProfileActivityRequest extractProfileActivityRequest = new TwitterExtractProfileActivityRequest(
                    relevanteAccount.getId(), networkProfileId);
            extractProfileActivityRequestRepo.save(extractProfileActivityRequest);
            queueService.sendBulkActionMessage(new BulkActionMessage(extractProfileActivityRequest));
        }
        if (isProfileDataStale(fullProfile)) {
            final TwitterExtractProfileDataRequest extractProfileDataRequest = new TwitterExtractProfileDataRequest(relevanteAccount.getId(),
                    networkProfileId);
            extractProfileDataRequestRepo.save(extractProfileDataRequest);
            queueService.sendBulkActionMessage(new BulkActionMessage(extractProfileDataRequest));
        }
    }

}
