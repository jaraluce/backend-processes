package me.relevante.backend.model;

import me.relevante.core.Linkedin;
import me.relevante.core.LinkedinFullProfile;
import me.relevante.core.LinkedinProfile;
import me.relevante.guesser.Clues;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Component;

@Component
public class LinkedinCluesPopulator implements NetworkCluesPopulator<Linkedin> {

    private final CrudRepository<LinkedinFullProfile, String> fullProfileRepo;

    @Autowired
    public LinkedinCluesPopulator(final CrudRepository<LinkedinFullProfile, String> fullProfileRepo) {
        this.fullProfileRepo = fullProfileRepo;
    }

    @Override
    public void populate(final Clues clues,
                         final String networkProfileId) {
        final LinkedinFullProfile fullProfile = fullProfileRepo.findOne(networkProfileId);
        final LinkedinProfile profile = fullProfile.getProfile();
        if (StringUtils.isBlank(clues.getName()) && StringUtils.isNotBlank(profile.getFirstName())) {
            clues.withName(profile.getFirstName(), profile.getLastName());
        }
        if (StringUtils.isBlank(clues.getCompany()) && StringUtils.isNotBlank(profile.getCompany())) {
            clues.withCompany(profile.getCompany());
        }
//        if (StringUtils.isBlank(clues.getDomain()) && !fullProfile.getPositions().isEmpty()) {
//            final String companyUrl = fullProfile.getPositions().get(0).getOrganizationUrl();
//            if (StringUtils.isNotBlank(companyUrl)) {
//                clues.withDomain(extractDomain(companyUrl));
//            }
//        }
    }

    @Override
    public Linkedin getNetwork() {
        return Linkedin.getInstance();
    }

    private String extractDomain(final String url) {
        final int atPosition = url.indexOf("@");
        return url.substring(atPosition + 1);
    }
}
