package me.relevante.model;

import me.relevante.core.Monitorable;
import org.springframework.stereotype.Component;

import java.util.Comparator;

/**
 * @author daniel-ibanez
 */
@Component
public class MonitoringComparator<T extends Monitorable> implements Comparator<T> {

    @Override
    public int compare(T o1, T o2) {
        if (o1.getLastMonitoringTimestamp() == null && o2.getLastMonitoringTimestamp() == null)
            return 0;
        if (o1.getLastMonitoringTimestamp() == null)
            return -1;
        if (o2.getLastMonitoringTimestamp() == null)
            return 1;
        return o1.getLastMonitoringTimestamp().compareTo(o2.getLastMonitoringTimestamp());
    }
}
