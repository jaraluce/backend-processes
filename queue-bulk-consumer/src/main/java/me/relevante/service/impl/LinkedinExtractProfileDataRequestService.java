package me.relevante.service.impl;

import me.relevante.core.Linkedin;
import me.relevante.core.LinkedinFullProfile;
import me.relevante.core.LinkedinPhoneNumber;
import me.relevante.core.LinkedinPosition;
import me.relevante.core.LinkedinProfile;
import me.relevante.core.RelevanteContext;
import me.relevante.core.UpdateStrategy;
import me.relevante.ingest.LinkedinIngestDate;
import me.relevante.ingest.LinkedinIngestExperienceOrganization;
import me.relevante.ingest.LinkedinIngestGroup;
import me.relevante.ingest.LinkedinIngestPositionItem;
import me.relevante.ingest.LinkedinIngestProfile;
import me.relevante.integration.LinkedinProfileIntegrator;
import me.relevante.persistence.BaseLinkedinFullProfileRepo;
import me.relevante.request.LinkedinExtractProfileDataRequest;
import me.relevante.service.NetworkBulkRequestService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.stream.Collectors;

@Service
public class LinkedinExtractProfileDataRequestService
        extends AbstractNetworkBulkRequestService<Linkedin, LinkedinExtractProfileDataRequest>
        implements NetworkBulkRequestService<Linkedin, LinkedinExtractProfileDataRequest> {

    private final BaseLinkedinFullProfileRepo fullProfileRepo;
    private final LinkedinProfileIntegrator profileIntegrator;

    @Autowired
    public LinkedinExtractProfileDataRequestService(final CrudRepository<LinkedinExtractProfileDataRequest, String> actionRequestRepo,
                                                    final CrudRepository<RelevanteContext, String> relevanteContextRepo,
                                                    final BaseLinkedinFullProfileRepo fullProfileRepo,
                                                    final LinkedinProfileIntegrator profileIntegrator) {
        super(actionRequestRepo, relevanteContextRepo);
        this.fullProfileRepo = fullProfileRepo;
        this.profileIntegrator = profileIntegrator;
    }

    @Override
    public Class<LinkedinExtractProfileDataRequest> getActionRequestClass() {
        return LinkedinExtractProfileDataRequest.class;
    }

    @Override
    public Linkedin getNetwork() {
        return Linkedin.getInstance();
    }

    @Override
    protected void execute(final LinkedinExtractProfileDataRequest extractProfileDataRequest) {
        if (extractProfileDataRequest.isNotSuccessful()) {
            return;
        }
        if (extractProfileDataRequest.getExtractedProfile() == null) {
            extractProfileDataRequest.setError();
            return;
        }
        final LinkedinFullProfile existingFullProfile = fullProfileRepo.findOne(extractProfileDataRequest.getTargetProfileId());
        final LinkedinFullProfile fullProfileFromIngestProfile = createFullProfileFromIngestProfile(extractProfileDataRequest.getExtractedProfile(), existingFullProfile.getProfile().getProfileUrl());
        profileIntegrator.integrate(fullProfileFromIngestProfile, UpdateStrategy.UPDATE_WITH_EXISTING_PROVIDED);
    }

    private LinkedinFullProfile createFullProfileFromIngestProfile(final LinkedinIngestProfile ingestProfile,
                                                                   final String profileUrl) {
        final LinkedinProfile profile = createProfileFromIngestProfile(ingestProfile, profileUrl);
        final LinkedinFullProfile fullProfile = new LinkedinFullProfile(profile);
        for (final LinkedinIngestGroup ingestGroup : ingestProfile.getGroups()) {
            if (ingestGroup.getProfile_url() == null || ingestGroup.getName() == null)
                continue;
            final String groupId = extractIdFromGroupUrl(ingestGroup.getProfile_url());
            if (groupId != null) {
                fullProfile.getGroupIds().add(groupId);
            }
        }
        for (final LinkedinIngestPositionItem ingestPositionItem : ingestProfile.getPositions()) {
            final LinkedinPosition position = new LinkedinPosition();
            position.setTitle(ingestPositionItem.getTitle());
            final LinkedinIngestDate endIngestDate = ingestPositionItem.getEnd();
            position.setEnd(endIngestDate == null ? null : String.valueOf(endIngestDate.getYear()) + " - " + String.valueOf(endIngestDate.getMonth()));
            position.setDescription(ingestPositionItem.getDescription());
            if (ingestPositionItem.getOrganization() != null) {
                position.setOrganizationName(ingestPositionItem.getOrganization().getName());
                position.setOrganizationUrl(ingestPositionItem.getOrganization().getProfile_url());
            }
            fullProfile.getPositions().add(position);
        }
        for (final String ingestSkill : ingestProfile.getSkills()) {
            fullProfile.getSkills().add(ingestSkill);
        }
        return fullProfile;
    }

    private String extractIdFromGroupUrl(final String groupUrl) {
        if (StringUtils.isBlank(groupUrl)) {
            return null;
        }
        int groupIdIndex = groupUrl.lastIndexOf("?gid=");
        if (groupIdIndex >= 0) {
            return groupUrl.substring(groupIdIndex + 5);
        }
        groupIdIndex = groupUrl.lastIndexOf("/");
        if (groupIdIndex >= 0) {
            return groupUrl.substring(groupIdIndex + 1);
        }
        return null;
    }


    private LinkedinProfile createProfileFromIngestProfile(final LinkedinIngestProfile ingestProfile,
                                                           final String profileUrl) {
        final LinkedinProfile profile = new LinkedinProfile();
        if (ingestProfile.getPositions() != null && !ingestProfile.getPositions().isEmpty()) {
            final LinkedinIngestPositionItem firstIngestPositionItem = ingestProfile.getPositions().get(0);
            final LinkedinIngestExperienceOrganization organization = firstIngestPositionItem.getOrganization();
            if (organization != null) {
                profile.setCompany(organization.getName());
            }
            profile.setPositionSummary(firstIngestPositionItem.getDescription());
            profile.setTitle(firstIngestPositionItem.getTitle());
        }
        profile.setId(ingestProfile.getLinkedin_id());
        profile.setFirstName(ingestProfile.getGiven_name());
        profile.setHeadline(ingestProfile.getHeadline());
        profile.setEmail(ingestProfile.getEmail());
        profile.setImageUrl(ingestProfile.getImage_url());
        profile.setIndustry(ingestProfile.getIndustry());
        profile.setLastName(ingestProfile.getFamily_name());
        profile.setLocation(ingestProfile.getLocality());
        profile.setName(ingestProfile.getFull_name());
        profile.setNumberConnections(ingestProfile.getNum_connections());
        profile.setProfileUrl(profileUrl);
        profile.setSummary(ingestProfile.getSummary());
        profile.setTwitterAccounts(ingestProfile.getTwitter() != null ? ingestProfile.getTwitter() : new ArrayList<>());
        if (ingestProfile.getPhoneNumbers() != null && !ingestProfile.getPhoneNumbers().isEmpty()) {
            profile.setPhoneNumbers(ingestProfile.getPhoneNumbers().stream()
                    .map(linkedinIngestPhoneNumber -> new LinkedinPhoneNumber(linkedinIngestPhoneNumber.getNumber(), linkedinIngestPhoneNumber.getType()))
                    .collect(Collectors.toList()));
        }
        if (StringUtils.isNotBlank(ingestProfile.getUpdated())) {
            try {
                profile.setUpdateTimestamp(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ").parse(ingestProfile.getUpdated()));
            } catch (ParseException e) {

            }
        }
        return profile;
    }
}
