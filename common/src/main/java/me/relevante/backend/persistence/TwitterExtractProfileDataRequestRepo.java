package me.relevante.backend.persistence;

import me.relevante.request.TwitterExtractProfileDataRequest;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/**
 * @author Daniel Ibanez
 */
@Repository
public interface TwitterExtractProfileDataRequestRepo extends MongoRepository<TwitterExtractProfileDataRequest, String> {
}
