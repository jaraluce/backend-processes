package me.relevante.service.impl;

import me.relevante.core.Linkedin;
import me.relevante.core.LinkedinFullProfile;
import me.relevante.core.LinkedinProfile;
import me.relevante.core.RelevanteContext;
import me.relevante.core.UniqueProfile;
import me.relevante.core.UpdateStrategy;
import me.relevante.integration.LinkedinProfileIntegrator;
import me.relevante.request.LinkedinExtractFeedProfilesRequest;
import me.relevante.request.LinkedinExtractProfileDataRequest;
import me.relevante.request.LinkedinExtractedFeedProfile;
import me.relevante.service.NetworkBulkRequestService;
import me.relevante.util.LinkedinUrlComposer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class LinkedinExtractFeedProfilesRequestService
        extends AbstractNetworkBulkRequestService<Linkedin, LinkedinExtractFeedProfilesRequest>
        implements NetworkBulkRequestService<Linkedin, LinkedinExtractFeedProfilesRequest> {

    private final LinkedinProfileIntegrator profileIntegrator;
    private final CrudRepository<LinkedinExtractProfileDataRequest, String> extractProfileDataRequestRepo;

    @Autowired
    public LinkedinExtractFeedProfilesRequestService(final CrudRepository<LinkedinExtractFeedProfilesRequest, String> actionRequestRepo,
                                                     final CrudRepository<RelevanteContext, String> relevanteContextRepo,
                                                     final LinkedinProfileIntegrator profileIntegrator,
                                                     final CrudRepository<LinkedinExtractProfileDataRequest, String> extractProfileDataRequestRepo) {
        super(actionRequestRepo, relevanteContextRepo);
        this.profileIntegrator = profileIntegrator;
        this.extractProfileDataRequestRepo = extractProfileDataRequestRepo;
    }

    @Override
    public Linkedin getNetwork() {
        return Linkedin.getInstance();
    }

    @Override
    public Class<LinkedinExtractFeedProfilesRequest> getActionRequestClass() {
        return LinkedinExtractFeedProfilesRequest.class;
    }

    @Override
    protected void execute(final LinkedinExtractFeedProfilesRequest extractFeedProfilesRequest) {
        if (extractFeedProfilesRequest.isNotSuccessful()) {
            return;
        }
        final List<LinkedinExtractedFeedProfile> extractedFeedProfiles = extractFeedProfilesRequest.getExtractedProfiles();
        for (final LinkedinExtractedFeedProfile extractedFeedProfile : extractedFeedProfiles) {
            final LinkedinFullProfile fullProfile = createFullProfileFromExtractedFeedProfile(extractedFeedProfile);
            final UniqueProfile uniqueProfile = profileIntegrator.integrate(fullProfile, UpdateStrategy.UPDATE_WITH_EXISTING_PROVIDED);
            createExtractProfileDataRequest(extractFeedProfilesRequest.getRequesterRelevanteId(), uniqueProfile);
        }
    }

    private LinkedinFullProfile createFullProfileFromExtractedFeedProfile(final LinkedinExtractedFeedProfile extractedFeedProfile) {
        final LinkedinProfile profile = createProfileFromExtractedFeedProfile(extractedFeedProfile);
        return new LinkedinFullProfile(profile);
    }

    private LinkedinProfile createProfileFromExtractedFeedProfile(final LinkedinExtractedFeedProfile extractedFeedProfile) {
        final LinkedinProfile profile = new LinkedinProfile();
        profile.setId(extractedFeedProfile.getPublicIdentifier());
        profile.setProfileUrl(LinkedinUrlComposer.toPublicProfileUrlfromPublicIdentifier(extractedFeedProfile.getPublicIdentifier()));
        profile.setFirstName(extractedFeedProfile.getFirstName());
        profile.setLastName(extractedFeedProfile.getLastName());
        profile.setName(extractedFeedProfile.getFirstName() + " " + extractedFeedProfile.getLastName());
        profile.setHeadline(extractedFeedProfile.getOccupation());
        return profile;
    }

    private LinkedinExtractProfileDataRequest createExtractProfileDataRequest(final String relevanteId,
                                                                              final UniqueProfile uniqueProfile) {
        final String linkedinProfileId = uniqueProfile.getProfileIdByNetwork(Linkedin.getInstance());
        final LinkedinExtractProfileDataRequest extractProfileDataRequest = new LinkedinExtractProfileDataRequest(relevanteId, linkedinProfileId);
        extractProfileDataRequestRepo.save(extractProfileDataRequest);
        return extractProfileDataRequest;
    }
}
