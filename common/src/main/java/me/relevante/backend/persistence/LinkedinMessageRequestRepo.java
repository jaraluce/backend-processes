package me.relevante.backend.persistence;

import me.relevante.persistence.NetworkActionRequestRepo;
import me.relevante.request.LinkedinMessageRequest;
import org.springframework.stereotype.Repository;

@Repository
public interface LinkedinMessageRequestRepo extends NetworkActionRequestRepo<LinkedinMessageRequest> {
}
