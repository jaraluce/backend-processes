package me.relevante.backend.model;

import me.relevante.core.CustomProfileData;
import me.relevante.core.NetworkProfileId;
import me.relevante.core.RelevanteProfileContext;
import me.relevante.core.UniqueProfile;
import me.relevante.guesser.Clues;
import me.relevante.service.RelevanteProfileContextService;
import me.relevante.util.MapByNetwork;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.Map;

@Component
public class CluesGenerator {

    private final CrudRepository<UniqueProfile, String> uniqueProfileRepo;
    private final RelevanteProfileContextService relevanteProfileContextService;
    private final Map<String, ? extends NetworkCluesPopulator> networkCluesPopulatorByNetworkName;

    @Autowired
    public CluesGenerator(final CrudRepository<UniqueProfile, String> uniqueProfileRepo,
                          final RelevanteProfileContextService relevanteProfileContextService,
                          final Collection<? extends NetworkCluesPopulator> networkCluesPopulators) {
        this.uniqueProfileRepo = uniqueProfileRepo;
        this.relevanteProfileContextService = relevanteProfileContextService;
        this.networkCluesPopulatorByNetworkName = MapByNetwork.from(networkCluesPopulators);
    }

    public Clues create(final String relevanteId,
                        final String uniqueProfileId) {
        final Clues clues = new Clues();
        final RelevanteProfileContext relevanteProfileContext = relevanteProfileContextService.getOrCreate(relevanteId, uniqueProfileId);
        if (relevanteProfileContext != null && relevanteProfileContext.getData() != null) {
            populateCluesFromCustomData(clues, relevanteProfileContext.getData());
        }
        final UniqueProfile uniqueProfile = uniqueProfileRepo.findOne(uniqueProfileId);
        for (final NetworkProfileId networkProfileId : uniqueProfile.getNetworkProfileIds()) {
            final NetworkCluesPopulator networkCluesPopulator = networkCluesPopulatorByNetworkName.get(networkProfileId.getNetwork());
            networkCluesPopulator.populate(clues, networkProfileId.getProfileId());
        }
        return clues;
    }

    private void populateCluesFromCustomData(final Clues clues,
                                             final CustomProfileData customProfileData) {
        if (StringUtils.isNotBlank(customProfileData.getName())) {
            clues.withName(customProfileData.getName(), "");
        }
    }
}
