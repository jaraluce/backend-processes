package me.relevante.backend.persistence;

import me.relevante.request.EnrichmentRequest;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EnrichmentRequestRepo extends MongoRepository<EnrichmentRequest, String> {
}
