package me.relevante.service.impl;

import me.relevante.service.NetworkBulkRequestService;
import me.relevante.core.Linkedin;
import me.relevante.core.LinkedinFullProfile;
import me.relevante.core.LinkedinMessage;
import me.relevante.core.RelevanteContext;
import me.relevante.core.UniqueProfile;
import me.relevante.request.ActionEngageStatus;
import me.relevante.request.LinkedinEngageStatus;
import me.relevante.request.LinkedinMessageRequest;
import me.relevante.service.RelevanteProfileContextService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

@Service
public class LinkedinMessageRequestService
        extends AbstractLinkedinEngageBulkRequestService<LinkedinMessageRequest, LinkedinMessage>
        implements NetworkBulkRequestService<Linkedin, LinkedinMessageRequest> {

    @Autowired
    public LinkedinMessageRequestService(final CrudRepository<LinkedinMessageRequest, String> messageRequestRepo,
                                         final CrudRepository<RelevanteContext, String> relevanteContextRepo,
                                         final CrudRepository<LinkedinMessage, String> messageRepo,
                                         final CrudRepository<UniqueProfile, String> uniqueProfileRepo,
                                         final CrudRepository<LinkedinFullProfile, String> fullProfileRepo,
                                         final RelevanteProfileContextService relevanteProfileContextService) {
        super(messageRequestRepo, relevanteContextRepo, messageRepo, uniqueProfileRepo, fullProfileRepo, relevanteProfileContextService);
    }

    @Override
    protected void updateActionEngageStatus(final LinkedinEngageStatus engageStatus,
                                            final ActionEngageStatus actionEngageStatus) {
        engageStatus.setMessageStatus(actionEngageStatus);
    }

    @Override
    public Class<LinkedinMessageRequest> getActionRequestClass() {
        return  LinkedinMessageRequest.class;
    }

    @Override
    protected LinkedinMessage createActionFromActionRequest(final LinkedinMessageRequest messageRequest) {
        return new LinkedinMessage(messageRequest.getRequesterRelevanteId(), messageRequest.getTargetProfileId(), messageRequest.getMessageText());
    }

}
