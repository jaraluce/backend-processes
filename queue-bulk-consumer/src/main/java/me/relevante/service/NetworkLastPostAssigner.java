package me.relevante.service;

import me.relevante.core.Network;
import me.relevante.core.NetworkEntity;

public interface NetworkLastPostAssigner<N extends Network> extends NetworkEntity<N> {

    void assignLastPost(final String networkProfileId, final String relevanteId);

}
