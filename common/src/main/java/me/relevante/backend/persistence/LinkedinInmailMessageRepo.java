package me.relevante.backend.persistence;

import me.relevante.core.LinkedinInmailMessage;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LinkedinInmailMessageRepo extends MongoRepository<LinkedinInmailMessage, String> {
	LinkedinInmailMessage findOneByTargetProfileIdAndAuthorId(String targetProfileId, String authorId);
}
