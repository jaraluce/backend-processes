package me.relevante.service;

import me.relevante.core.Network;
import me.relevante.core.NetworkEntity;
import me.relevante.queue.BulkActionMessage;
import me.relevante.request.NetworkActionRequest;

public interface NetworkBulkRequestService<N extends Network, R extends NetworkActionRequest<N>> extends NetworkEntity<N> {
    BulkActionMessage process(BulkActionMessage message);
    Class<R> getActionRequestClass();
}
