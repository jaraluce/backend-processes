package me.relevante.service.impl;

import me.relevante.service.NetworkBulkRequestService;
import me.relevante.core.Linkedin;
import me.relevante.core.LinkedinFullProfile;
import me.relevante.core.LinkedinLike;
import me.relevante.core.RelevanteContext;
import me.relevante.core.UniqueProfile;
import me.relevante.request.ActionEngageStatus;
import me.relevante.request.LinkedinEngageStatus;
import me.relevante.request.LinkedinLikeRequest;
import me.relevante.service.RelevanteProfileContextService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

@Service
public class LinkedinLikeRequestService
        extends AbstractLinkedinEngageBulkRequestService<LinkedinLikeRequest, LinkedinLike>
        implements NetworkBulkRequestService<Linkedin, LinkedinLikeRequest> {

    @Autowired
    public LinkedinLikeRequestService(final CrudRepository<LinkedinLikeRequest, String> likeRequestRepo,
                                      final CrudRepository<RelevanteContext, String> relevanteContextRepo,
                                      final CrudRepository<LinkedinLike, String> likeRepo,
                                      final CrudRepository<UniqueProfile, String> uniqueProfileRepo,
                                      final CrudRepository<LinkedinFullProfile, String> fullProfileRepo,
                                      final RelevanteProfileContextService relevanteProfileContextService) {
        super(likeRequestRepo, relevanteContextRepo, likeRepo, uniqueProfileRepo, fullProfileRepo, relevanteProfileContextService);
    }

    @Override
    protected void updateActionEngageStatus(final LinkedinEngageStatus engageStatus,
                                            final ActionEngageStatus actionEngageStatus) {
        engageStatus.setLikeStatus(actionEngageStatus);
    }

    @Override
    public Class<LinkedinLikeRequest> getActionRequestClass() {
        return  LinkedinLikeRequest.class;
    }

    @Override
    protected LinkedinLike createActionFromActionRequest(final LinkedinLikeRequest likeRequest) {
        return new LinkedinLike(likeRequest.getRequesterRelevanteId(), likeRequest.getTargetProfileId(), likeRequest.getTargetPostId());
    }
}
