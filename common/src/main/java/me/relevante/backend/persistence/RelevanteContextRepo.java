package me.relevante.backend.persistence;

import me.relevante.core.RelevanteContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RelevanteContextRepo extends MongoRepository<RelevanteContext, String> {
}
