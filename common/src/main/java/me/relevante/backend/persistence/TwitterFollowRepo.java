package me.relevante.backend.persistence;

import me.relevante.core.TwitterFollow;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TwitterFollowRepo extends MongoRepository<TwitterFollow, String> {
	TwitterFollow findOneByTargetProfileIdAndAuthorId(String targetProfileid, String authorId);
}
