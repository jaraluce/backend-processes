package me.relevante.service.impl;

import me.relevante.service.NetworkBulkRequestService;
import me.relevante.core.Network;
import me.relevante.core.NetworkEngageAction;
import me.relevante.core.NetworkFullProfile;
import me.relevante.core.RelevanteContext;
import me.relevante.core.RelevanteProfileContext;
import me.relevante.core.UniqueProfile;
import me.relevante.request.ActionEngageStatus;
import me.relevante.request.NetworkEngageActionRequest;
import me.relevante.request.NetworkEngageStatus;
import me.relevante.service.RelevanteProfileContextService;
import org.springframework.data.repository.CrudRepository;

public abstract class AbstractNetworkEngageBulkRequestService<N extends Network, R extends NetworkEngageActionRequest<N>, A extends NetworkEngageAction<N>, F extends NetworkFullProfile<N, ?, F>, E extends NetworkEngageStatus<N, E>>
        extends AbstractNetworkBulkRequestService<N, R>
        implements NetworkBulkRequestService<N, R> {

    protected final CrudRepository<A, String> actionRepo;
    protected final CrudRepository<UniqueProfile, String> uniqueProfileRepo;
    protected final CrudRepository<F, String> fullProfileRepo;
    private final RelevanteProfileContextService relevanteProfileContextService;

    public AbstractNetworkEngageBulkRequestService(final CrudRepository<R, String> actionRequestRepo,
                                                   final CrudRepository<RelevanteContext, String> relevanteContextRepo,
                                                   final CrudRepository<A, String> actionRepo,
                                                   final CrudRepository<UniqueProfile, String> uniqueProfileRepo,
                                                   final CrudRepository<F, String> fullProfileRepo,
                                                   final RelevanteProfileContextService relevanteProfileContextService) {
        super(actionRequestRepo, relevanteContextRepo);
        this.actionRepo = actionRepo;
        this.uniqueProfileRepo = uniqueProfileRepo;
        this.fullProfileRepo = fullProfileRepo;
        this.relevanteProfileContextService = relevanteProfileContextService;
    }

    protected void execute(final R actionRequest) {
        executeActionRequest(actionRequest);
        if (actionRequest.isNotSuccessful()) {
            return;
        }
        updateRelatedAction(actionRequest);
        final RelevanteProfileContext relevanteProfileContext = relevanteProfileContextService.getOrCreate(actionRequest.getRequesterRelevanteId(), actionRequest.getTargetProfileId());
        updateRelevanteProfileContextEngageStatus(relevanteProfileContext, actionRequest);
        relevanteProfileContextService.save(relevanteProfileContext);
    }

    protected abstract A createActionFromActionRequest(final R actionRequest);

    protected abstract E createEngageStatusForCurrentNetwork();

    protected abstract void updateActionEngageStatus(final E engageStatus, final ActionEngageStatus actionRequest);

    protected abstract void executeActionRequest(R actionRequest);

    private void updateRelevanteProfileContextEngageStatus(final RelevanteProfileContext relevanteProfileContext,
                                                           final R actionRequest) {
        E engageStatus = relevanteProfileContext.getEngageStatus(getNetwork().getName());
        if (engageStatus == null) {
            engageStatus = createEngageStatusForCurrentNetwork();
            relevanteProfileContext.setEngageStatus(engageStatus);
        }
        final ActionEngageStatus actionEngageStatus = new ActionEngageStatus(actionRequest.getStatus(), actionRequest.getResult());
        updateActionEngageStatus(engageStatus, actionEngageStatus);
    }

    private void updateRelatedAction(R actionRequest) {
        if (actionRequest.isAlreadySuceeded() || actionRequest.isSuccessful()) {
            final A action = createActionFromActionRequest(actionRequest);
            actionRepo.save(action);
        }
    }
}
