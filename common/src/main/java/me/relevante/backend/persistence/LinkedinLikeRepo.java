package me.relevante.backend.persistence;

import me.relevante.core.LinkedinLike;
import me.relevante.persistence.NetworkPostActionRepo;
import org.springframework.stereotype.Repository;

@Repository
public interface LinkedinLikeRepo extends NetworkPostActionRepo<LinkedinLike> {
}
