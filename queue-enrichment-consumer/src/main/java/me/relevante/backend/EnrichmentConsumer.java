
package me.relevante.backend;

import me.relevante.backend.queue.QueueName;
import me.relevante.backend.service.EnrichmentService;
import me.relevante.queue.EnrichmentMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class EnrichmentConsumer implements Consumer<EnrichmentMessage> {

    private static final Logger logger = LoggerFactory.getLogger(EnrichmentConsumer.class);

    private final EnrichmentService enrichmentService;

    @Autowired
    public EnrichmentConsumer(final EnrichmentService enrichmentService) {
        this.enrichmentService = enrichmentService;
    }

    @RabbitListener(queues = QueueName.ENRICHMENT)
	public void process(final EnrichmentMessage request) {
        try {
            enrichmentService.process(request);
        } catch (Exception e) {
            logger.error("Error processing EnrichmentMessage", e);
        }
    }

}