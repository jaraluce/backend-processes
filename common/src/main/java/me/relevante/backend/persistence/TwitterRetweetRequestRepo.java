package me.relevante.backend.persistence;

import me.relevante.persistence.NetworkActionRequestRepo;
import me.relevante.request.TwitterRetweetRequest;
import org.springframework.stereotype.Repository;

@Repository
public interface TwitterRetweetRequestRepo extends NetworkActionRequestRepo<TwitterRetweetRequest> {
}
