package me.relevante.backend.queue;

import me.relevante.guesser.ClearbitDomainGuesser;
import me.relevante.guesser.domain.DomainGuesser;
import me.relevante.guesser.domain.FullContactDomainGuesser;
import me.relevante.guesser.email.EmailGuesser;
import me.relevante.guesser.email.VoilaNorbertEmailGuesser;
import me.relevante.guesser.profile.FullContactProfileGuesser;
import me.relevante.guesser.profile.ProfileGuesser;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Arrays;
import java.util.Collection;

@Configuration
public class BaseRelevanteConfig {

    @Bean
    public Collection<? extends DomainGuesser> providerDomainGuessers(final ClearbitDomainGuesser clearbitDomainGuesser,
                                                                      final FullContactDomainGuesser fullContactDomainGuesser) {
        return Arrays.asList(clearbitDomainGuesser, fullContactDomainGuesser);
    }

    @Bean
    public Collection<? extends ProfileGuesser> providerProfileGuessers(final FullContactProfileGuesser fullContactProfileGuesser) {
        return Arrays.asList(fullContactProfileGuesser);
    }

    @Bean
    public Collection<? extends EmailGuesser> providerEmailGuessers(final VoilaNorbertEmailGuesser voilaNorbertEmailGuesser) {
        return Arrays.asList(voilaNorbertEmailGuesser);
    }

}