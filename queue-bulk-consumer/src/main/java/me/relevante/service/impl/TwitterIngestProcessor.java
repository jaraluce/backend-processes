package me.relevante.service.impl;

import me.relevante.backend.persistence.TwitterFullPostRepo;
import me.relevante.core.TwitterFullPost;
import me.relevante.core.TwitterFullProfile;
import me.relevante.core.TwitterPost;
import me.relevante.core.TwitterProfile;
import me.relevante.core.UniqueProfile;
import me.relevante.core.UpdateStrategy;
import me.relevante.ingest.TwitterIngestPost;
import me.relevante.ingest.TwitterIngestProfile;
import me.relevante.integration.TwitterProfileIntegrator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class TwitterIngestProcessor {

    private final TwitterFullPostRepo fullPostRepo;
    private final TwitterProfileIntegrator profileIntegrator;

    @Autowired
    public TwitterIngestProcessor(final TwitterFullPostRepo fullPostRepo,
                                  final TwitterProfileIntegrator profileIntegrator) {
        this.fullPostRepo = fullPostRepo;
        this.profileIntegrator = profileIntegrator;
    }

    public UniqueProfile processIngestProfile(final TwitterIngestProfile ingestProfile) {
        if (ingestProfile == null) {
            return null;
        }
        final TwitterFullProfile fullProfileFromIngestProfile = createFullProfileFromIngestProfile(ingestProfile);
        return profileIntegrator.integrate(fullProfileFromIngestProfile, UpdateStrategy.UPDATE_WITH_EXISTING_PROVIDED);
    }

    public void processIngestPost(final TwitterIngestPost ingestPost) {
        if (ingestPost == null) {
            return;
        }
        final TwitterFullPost fullPostFromIngestPost = createFullPostFromIngestPost(ingestPost);
        final TwitterFullPost existingFullPost = fullPostRepo.findOneByPostId(fullPostFromIngestPost.getPost().getId());
        if (existingFullPost == null) {
            fullPostRepo.save(fullPostFromIngestPost);
        } else {
            existingFullPost.updateWithExistingDataIn(fullPostFromIngestPost);
            fullPostRepo.save(existingFullPost);
        }
    }

    private TwitterFullProfile createFullProfileFromIngestProfile(final TwitterIngestProfile ingestProfile) {
        final TwitterProfile profile = createProfileFromIngestProfile(ingestProfile);
        final TwitterFullProfile fullProfile = new TwitterFullProfile(profile);
        return fullProfile;
    }

    private TwitterProfile createProfileFromIngestProfile(TwitterIngestProfile ingestProfile) {
        final TwitterProfile profile = new TwitterProfile();
        profile.setTwitterId(ingestProfile.getId());
        profile.setBioDescription(ingestProfile.getBioDescription());
        profile.setBioUrl(ingestProfile.getBioUrl());
        profile.setDescription(ingestProfile.getDescription());
        profile.setFollowers(ingestProfile.getFollowers());
        profile.setFollowing(ingestProfile.getFollowing());
        profile.setImageUrl(ingestProfile.getImageUrl());
        profile.setLocation(ingestProfile.getLocation());
        profile.setName(ingestProfile.getName());
        profile.setProfileUrl(ingestProfile.getProfileUrl());
        profile.setScreenName(ingestProfile.getScreenName());
        return profile;
    }

    private TwitterFullPost createFullPostFromIngestPost(final TwitterIngestPost ingestPost) {
        final TwitterPost postFromIngestShare = createPostFromIngestPost(ingestPost);
        final TwitterFullPost fullPostFromIngestShare = new TwitterFullPost(postFromIngestShare);
        return fullPostFromIngestShare;
    }

    private TwitterPost createPostFromIngestPost(final TwitterIngestPost ingestPost) {
        final TwitterPost post = new TwitterPost();
        post.setTwitterId(Long.valueOf(ingestPost.getId()));
        post.setAuthorTwitterId(ingestPost.getAuthorId());
        post.setCreationTimestamp(ingestPost.getCreationTimestamp());
        post.setLanguage(ingestPost.getLanguage());
        post.setLatitude(ingestPost.getLatitude());
        post.setLongitude(ingestPost.getLongitude());
        post.setText(ingestPost.getText());
        post.setTweetUrl(ingestPost.getTweetUrl());
        return post;
    }

}
