package me.relevante.service.impl;

import me.relevante.api.TwitterApiClient;
import me.relevante.api.TwitterApiException;
import me.relevante.api.TwitterApiResponse;
import me.relevante.api.TwitterApiResult;
import me.relevante.service.NetworkBulkRequestService;
import me.relevante.core.RelevanteAccount;
import me.relevante.core.RelevanteContext;
import me.relevante.core.Twitter;
import me.relevante.core.TwitterDirectMessage;
import me.relevante.core.TwitterFullProfile;
import me.relevante.core.UniqueProfile;
import me.relevante.request.ActionEngageStatus;
import me.relevante.request.TwitterDirectMessageRequest;
import me.relevante.request.TwitterEngageStatus;
import me.relevante.service.RelevanteProfileContextService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

@Service
public class TwitterDirectMessageRequestService
        extends AbstractTwitterEngageBulkRequestService<TwitterDirectMessageRequest, TwitterDirectMessage>
        implements NetworkBulkRequestService<Twitter, TwitterDirectMessageRequest> {

    @Autowired
    public TwitterDirectMessageRequestService(final CrudRepository<TwitterDirectMessageRequest, String> directMessageRequestRepo,
                                              final CrudRepository<RelevanteContext, String> relevanteContextRepo,
                                              final CrudRepository<TwitterDirectMessage, String> directMessageRepo,
                                              final CrudRepository<UniqueProfile, String> uniqueProfileRepo,
                                              final CrudRepository<TwitterFullProfile, String> fullProfileRepo,
                                              final RelevanteProfileContextService relevanteProfileContextService,
                                              final CrudRepository<RelevanteAccount, String> relevanteAccountRepo) {
        super(directMessageRequestRepo, relevanteContextRepo, directMessageRepo, uniqueProfileRepo, fullProfileRepo, relevanteProfileContextService,
                relevanteAccountRepo);
    }

    @Override
    public Class<TwitterDirectMessageRequest> getActionRequestClass() {
        return TwitterDirectMessageRequest.class;
    }

    @Override
    protected TwitterDirectMessage createActionFromActionRequest(final TwitterDirectMessageRequest directMessageRequest) {
        return new TwitterDirectMessage(directMessageRequest.getRequesterRelevanteId(), directMessageRequest.getTargetProfileId(),
                directMessageRequest.getMessage());
    }

    @Override
    protected TwitterApiResponse executeApiAction(final TwitterDirectMessageRequest directMessageRequest, final TwitterApiClient apiClient) {
        try {
            return apiClient.sendDirectMessage(directMessageRequest.getTargetProfileId(), directMessageRequest.getMessage());
        } catch (final TwitterApiException e) {
            return new TwitterApiResponse(TwitterApiResult.FORBIDDEN, e);
        }
    }

    @Override
    protected void updateActionEngageStatus(final TwitterEngageStatus engageStatus, final ActionEngageStatus actionEngageStatus) {
        engageStatus.setDirectMessageStatus(actionEngageStatus);
    }

}
