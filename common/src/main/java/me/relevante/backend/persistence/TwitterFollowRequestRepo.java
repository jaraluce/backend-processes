package me.relevante.backend.persistence;

import me.relevante.persistence.NetworkActionRequestRepo;
import me.relevante.request.TwitterFollowRequest;
import org.springframework.stereotype.Repository;

@Repository
public interface TwitterFollowRequestRepo extends NetworkActionRequestRepo<TwitterFollowRequest> {
}
