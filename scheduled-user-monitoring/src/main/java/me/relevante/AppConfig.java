package me.relevante;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.rabbitmq.client.ConnectionFactory;
import me.relevante.auth.OAuthKeyPair;
import me.relevante.core.Linkedin;
import me.relevante.core.Twitter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AppConfig {

    @Bean
    public ObjectMapper jsonMapper() {
        return new ObjectMapper();
    }

    @Bean
    public ConnectionFactory rabbitMQConnectionFactory(@Value("${spring.rabbitmq.host}") final String rabbitHost,
                                                       @Value("${spring.rabbitmq.port}") final int rabbitPort,
                                                       @Value("${spring.rabbitmq.username}") final String rabbitUsername,
                                                       @Value("${spring.rabbitmq.password}") final String rabbitPassword,
                                                       @Value("${spring.rabbitmq.virtualHost}") final String rabbitVirtualHost) {
        if (rabbitPort == 0) {
            return null;
        }
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost(rabbitHost);
        factory.setPort(rabbitPort);
        factory.setUsername(rabbitUsername);
        factory.setPassword(rabbitPassword);
        factory.setVirtualHost(rabbitVirtualHost);
        return factory;
    }

    @Bean
    public OAuthKeyPair<Linkedin> linkedinMainOAuthKeyPair(@Value("${linkedin.oAuthCredentials.main.apiKey}") String key,
                                                           @Value("${linkedin.oAuthCredentials.main.apiSecret}") String secret) {
        return new OAuthKeyPair<>(key, secret);
    }

    @Bean
    public OAuthKeyPair<Twitter> twitterMainOAuthKeyPair(@Value("${twitter.oAuthCredentials.main.apiKey}") String key,
                                                         @Value("${twitter.oAuthCredentials.main.apiSecret}") String secret) {
        return new OAuthKeyPair<>(key, secret);
    }

    @Bean(name = "nlpResourcesPath")
    public String getNlpResourcesPath(@Value("${nlp.resources.path}") final String nlpResourcesPath) {
        return nlpResourcesPath;
    }

}