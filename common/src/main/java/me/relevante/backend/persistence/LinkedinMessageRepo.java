package me.relevante.backend.persistence;

import me.relevante.core.LinkedinMessage;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LinkedinMessageRepo extends MongoRepository<LinkedinMessage, String> {
	LinkedinMessage findOneByTargetProfileIdAndAuthorId(String targetProfileId, String authorId);
}
