package me.relevante.backend.persistence;

import me.relevante.core.LinkedinFullPost;
import me.relevante.persistence.NetworkFindByIdsRepo;
import me.relevante.persistence.NetworkFullPostRepo;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;

@Repository
public interface LinkedinFullPostRepo extends NetworkFullPostRepo<LinkedinFullPost>, NetworkFindByIdsRepo<LinkedinFullPost> {
    List<LinkedinFullPost> findByPostAuthorIdIn(Collection<String> authorIds);
    LinkedinFullPost findOneByPostAuthorId(String authorId);
    LinkedinFullPost findOneByPostId(String postId);
}
