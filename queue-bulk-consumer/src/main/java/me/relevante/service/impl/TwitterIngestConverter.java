package me.relevante.service.impl;

import me.relevante.core.TwitterPost;
import me.relevante.core.TwitterProfile;
import me.relevante.ingest.TwitterIngestPost;
import me.relevante.ingest.TwitterIngestProfile;
import org.springframework.stereotype.Component;

@Component
public class TwitterIngestConverter {

    public TwitterIngestProfile createIngestProfileFromProfile(final TwitterProfile profile) {
        TwitterIngestProfile ingestProfile = new TwitterIngestProfile();
        ingestProfile.setId(profile.getTwitterId());
        ingestProfile.setBioDescription(profile.getBioDescription());
        ingestProfile.setBioUrl(profile.getBioUrl());
        ingestProfile.setDescription(profile.getDescription());
        ingestProfile.setFollowers(profile.getFollowers());
        ingestProfile.setFollowing(profile.getFollowing());
        ingestProfile.setImageUrl(profile.getImageUrl());
        ingestProfile.setLocation(profile.getLocation());
        ingestProfile.setName(profile.getName());
        ingestProfile.setProfileUrl(profile.getProfileUrl());
        ingestProfile.setScreenName(profile.getScreenName());
        return ingestProfile;
    }

    public TwitterIngestPost createIngestPostFromPost(final TwitterPost post) {
        TwitterIngestPost ingestPost = new TwitterIngestPost();
        ingestPost.setId(post.getTwitterId());
        ingestPost.setAuthorId(post.getAuthorId());
        ingestPost.setCreationTimestamp(post.getCreationTimestamp());
        ingestPost.setLanguage(post.getLanguage());
        ingestPost.setLatitude(post.getLatitude());
        ingestPost.setLongitude(post.getLongitude());
        ingestPost.setText(post.getText());
        ingestPost.setTweetUrl(post.getTweetUrl());
        return ingestPost;
    }

}
