package me.relevante.backend.persistence;

import me.relevante.request.LinkedinExtractProfileDataRequest;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/**
 * @author Daniel Ibanez
 */
@Repository
public interface LinkedinExtractProfileDataRequestRepo extends MongoRepository<LinkedinExtractProfileDataRequest, String> {
}
