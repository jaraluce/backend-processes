
package me.relevante;

import me.relevante.backend.Consumer;
import me.relevante.backend.queue.QueueName;
import me.relevante.queue.DeduplicationMessage;
import me.relevante.service.DeduplicationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class DeduplicationConsumer implements Consumer<DeduplicationMessage> {

    private static final Logger logger = LoggerFactory.getLogger(DeduplicationConsumer.class);

    private final DeduplicationService deduplicationService;

    @Autowired
    public DeduplicationConsumer(final DeduplicationService deduplicationService) {
        this.deduplicationService = deduplicationService;
    }

    @RabbitListener(queues = QueueName.DEDUPLICATION)
	public void process(final DeduplicationMessage request) {
        try {
            deduplicationService.process(request);
        } catch (Exception e) {
            logger.error("Error processing Deduplication request " + request, e);
        }
    }

}