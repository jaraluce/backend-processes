package me.relevante.backend.persistence;

import me.relevante.persistence.NetworkActionRequestRepo;
import me.relevante.request.TwitterDirectMessageRequest;
import org.springframework.stereotype.Repository;

@Repository
public interface TwitterDirectMessageRequestRepo extends NetworkActionRequestRepo<TwitterDirectMessageRequest> {
}
