package me.relevante.backend.persistence;

import me.relevante.core.TwitterReply;
import me.relevante.persistence.NetworkPostActionRepo;
import org.springframework.stereotype.Repository;

@Repository
public interface TwitterReplyRepo extends NetworkPostActionRepo<TwitterReply> {
}
