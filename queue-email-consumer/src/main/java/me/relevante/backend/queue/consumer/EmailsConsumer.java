package me.relevante.backend.queue.consumer;

import me.relevante.backend.Consumer;
import me.relevante.backend.queue.QueueName;
import me.relevante.queue.SendEmailMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class EmailsConsumer implements Consumer<SendEmailMessage> {

    private static final Logger logger = LoggerFactory.getLogger(EmailsConsumer.class);

    private final MailSender mailSender;

    @Autowired
    public EmailsConsumer(final MailSender mailSender) {
        this.mailSender = mailSender;
    }

    @RabbitListener(queues = QueueName.SEND_EMAIL)
    public void process(final SendEmailMessage request) {
        try {
            mailSender.sendMail(request);
        }
        catch (final Exception e) {
            logger.error("Error processing Twitter bulk action", e);
        }
    }
}