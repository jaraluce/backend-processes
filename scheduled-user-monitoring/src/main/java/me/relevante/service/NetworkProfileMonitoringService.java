package me.relevante.service;

import me.relevante.core.Network;
import me.relevante.core.NetworkEntity;
import me.relevante.core.RelevanteAccount;

import java.util.List;

/**
 * @author daniel-ibanez
 */
public interface NetworkProfileMonitoringService<N extends Network> extends NetworkEntity<N> {
    void processProfile(String networkProfileId, RelevanteAccount relevanteAccount);
    void processSearchTerms(List<String> searchTerms, RelevanteAccount relevanteAccount);
}
