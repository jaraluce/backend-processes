package me.relevante.backend.persistence;

import me.relevante.core.PocketItem;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PocketItemRepo extends MongoRepository<PocketItem, String> {
}
