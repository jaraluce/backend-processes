package me.relevante.backend.persistence;

import me.relevante.hub.PocketWordpressMapping;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PocketWordpressMappingRepo extends MongoRepository<PocketWordpressMapping, ObjectId> {
}
