package me.relevante.backend.queue.consumer;

import me.relevante.queue.SendEmailMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Component;

@Component
public class MailSender {

	private final JavaMailSender javaMailSender;

	@Autowired
	public MailSender(final JavaMailSender javaMailSender) {
		this.javaMailSender = javaMailSender;
	}

	public void sendMail(String from, String to, String subject, String msg) {
		SimpleMailMessage message = new SimpleMailMessage();
		message.setFrom(from);
		message.setTo(to.split(","));
		message.setSubject(subject);
		message.setText(msg);
		javaMailSender.send(message);
	}

	public void sendMail(SendEmailMessage sendEmailRequest) {
		this.sendMail(sendEmailRequest.getFrom(), sendEmailRequest.getTo(), sendEmailRequest.getSubject(), sendEmailRequest.getContent());
	}
}
