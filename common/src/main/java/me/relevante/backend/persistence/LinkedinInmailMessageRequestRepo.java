package me.relevante.backend.persistence;

import me.relevante.persistence.NetworkActionRequestRepo;
import me.relevante.request.LinkedinInmailMessageRequest;
import org.springframework.stereotype.Repository;

@Repository
public interface LinkedinInmailMessageRequestRepo extends NetworkActionRequestRepo<LinkedinInmailMessageRequest> {
}
