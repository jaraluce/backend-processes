package me.relevante.backend.service;

import me.relevante.backend.model.CluesGenerator;
import me.relevante.backend.persistence.DeduplicationRequestRepo;
import me.relevante.backend.persistence.EnrichmentRequestRepo;
import me.relevante.backend.persistence.UniqueProfileRepo;
import me.relevante.core.RelevanteProfileContext;
import me.relevante.core.ScoredEmail;
import me.relevante.core.UniqueProfile;
import me.relevante.guesser.Clues;
import me.relevante.guesser.email.EmailGuesser;
import me.relevante.guesser.email.GuessedEmailWithProfiles;
import me.relevante.queue.DeduplicationMessage;
import me.relevante.queue.EnrichmentMessage;
import me.relevante.queue.QueueService;
import me.relevante.request.ActionEngageStatus;
import me.relevante.request.DeduplicationRequest;
import me.relevante.request.EnrichmentRequest;
import me.relevante.service.RelevanteProfileContextService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
public class EnrichmentService {

    private final CluesGenerator cluesGenerator;
    private final EmailGuesser emailGuesser;
    private final UniqueProfileRepo uniqueProfileRepo;
    private final EnrichmentRequestRepo enrichmentRequestRepo;
    private final RelevanteProfileContextService relevanteProfileContextService;
    private final DeduplicationRequestRepo deduplicationRequestRepo;
    private final QueueService queueService;

    @Autowired
    public EnrichmentService(final CluesGenerator cluesGenerator,
                             final EmailGuesser emailGuesser,
                             final UniqueProfileRepo uniqueProfileRepo,
                             final EnrichmentRequestRepo enrichmentRequestRepo,
                             final RelevanteProfileContextService relevanteProfileContextService,
                             final DeduplicationRequestRepo deduplicationRequestRepo,
                             final QueueService queueService) {
        this.cluesGenerator = cluesGenerator;
        this.emailGuesser = emailGuesser;
        this.uniqueProfileRepo = uniqueProfileRepo;
        this.enrichmentRequestRepo = enrichmentRequestRepo;
        this.relevanteProfileContextService = relevanteProfileContextService;
        this.deduplicationRequestRepo = deduplicationRequestRepo;
        this.queueService = queueService;
    }

    public void process(final EnrichmentMessage request) {
        final me.relevante.request.EnrichmentRequest enrichmentRequest = enrichmentRequestRepo.findOne(request.getRequestId());
        guessEmailsIntoRequest(enrichmentRequest);
        enrichmentRequest.setSuccess();
        enrichmentRequestRepo.save(enrichmentRequest);
        updateUniqueProfile(enrichmentRequest.getTargetProfileId(), enrichmentRequest.getObtainedEmails());
        updateRelevanteProfileContext(enrichmentRequest);
        deduplicateUniqueProfile(request.getRelevanteId(), request.getTargetUniqueProfileId());
    }

    private final void guessEmailsIntoRequest(final EnrichmentRequest enrichmentRequest) {
        final Clues clues = cluesGenerator.create(enrichmentRequest.getRelevanteId(), enrichmentRequest.getTargetProfileId());
        final List<GuessedEmailWithProfiles> guessedEmailsWithProfiles = emailGuesser.guessWithProfiles(clues);
        guessedEmailsWithProfiles.sort((o1, o2) -> new Integer(o2.getScore()).compareTo(new Integer(o1.getScore())));
        guessedEmailsWithProfiles.forEach(guessedEmail -> enrichmentRequest.addObtainedEmail(createScoredEmailFromGuessedEmail(guessedEmail)));
        guessedEmailsWithProfiles.forEach(guessedEmail -> enrichmentRequest.addObtainedProfiles(guessedEmail.getGuessedProfiles()));
    }

    private final ScoredEmail createScoredEmailFromGuessedEmail(final GuessedEmailWithProfiles guessedEmail) {
        final double score = ((double) guessedEmail.getScore()) / 100D;
        return new ScoredEmail(guessedEmail.getEmail(), score);
    }

    private void updateUniqueProfile(final String uniqueProfileId,
                                     final List<ScoredEmail> newEmails) {
        final UniqueProfile uniqueProfile = uniqueProfileRepo.findOne(uniqueProfileId);
        final Map<String, ScoredEmail> profileEmailsMap = uniqueProfile.getScoredEmails().stream().collect(Collectors.toMap(ScoredEmail::getEmail, Function.identity()));
        for (final ScoredEmail newScoredEmail : newEmails) {
            if (!profileEmailsMap.containsKey(newScoredEmail.getEmail())) {
                profileEmailsMap.put(newScoredEmail.getEmail(), newScoredEmail);
            }
        }
        uniqueProfile.setEmails(profileEmailsMap.values());
        uniqueProfileRepo.save(uniqueProfile);
    }

    private void updateRelevanteProfileContext(final EnrichmentRequest enrichmentRequest) {
        final RelevanteProfileContext relevanteProfileContext = relevanteProfileContextService.getOrCreate(enrichmentRequest.getRelevanteId(),
                enrichmentRequest.getTargetProfileId());
        relevanteProfileContext.setEnrichmentStatus(new ActionEngageStatus(enrichmentRequest.getStatus(), enrichmentRequest.getResult()));
        relevanteProfileContextService.save(relevanteProfileContext);
    }

    private void deduplicateUniqueProfile(final String relevanteId,
                                          final String uniqueProfileId) {
        final DeduplicationRequest deduplicationRequest = new DeduplicationRequest(relevanteId, uniqueProfileId);
        final DeduplicationRequest deduplicationRequestWithId = deduplicationRequestRepo.save(deduplicationRequest);
        queueService.sendDeduplicationMessage(new DeduplicationMessage(deduplicationRequestWithId.getId(), deduplicationRequestWithId.getTargetProfileId()));
    }
}
