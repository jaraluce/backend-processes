package me.relevante.service.impl;

import me.relevante.service.NetworkBulkRequestService;
import me.relevante.core.Linkedin;
import me.relevante.core.LinkedinConnect;
import me.relevante.core.LinkedinFullProfile;
import me.relevante.core.RelevanteContext;
import me.relevante.core.UniqueProfile;
import me.relevante.request.ActionEngageStatus;
import me.relevante.request.LinkedinConnectRequest;
import me.relevante.request.LinkedinEngageStatus;
import me.relevante.service.RelevanteProfileContextService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

@Service
public class LinkedinConnectRequestService
        extends AbstractLinkedinEngageBulkRequestService<LinkedinConnectRequest, LinkedinConnect>
        implements NetworkBulkRequestService<Linkedin, LinkedinConnectRequest> {

    @Autowired
    public LinkedinConnectRequestService(final CrudRepository<LinkedinConnectRequest, String> commentRequestRepo,
                                         final CrudRepository<RelevanteContext, String> relevanteContextRepo,
                                         final CrudRepository<LinkedinConnect, String> connectRepo,
                                         final CrudRepository<UniqueProfile, String> uniqueProfileRepo,
                                         final CrudRepository<LinkedinFullProfile, String> fullProfileRepo,
                                         final RelevanteProfileContextService relevanteProfileContextService) {
        super(commentRequestRepo, relevanteContextRepo, connectRepo, uniqueProfileRepo, fullProfileRepo, relevanteProfileContextService);
    }

    @Override
    protected void updateActionEngageStatus(final LinkedinEngageStatus engageStatus,
                                            final ActionEngageStatus actionEngageStatus) {
        engageStatus.setConnectStatus(actionEngageStatus);
    }

    @Override
    public Class<LinkedinConnectRequest> getActionRequestClass() {
        return  LinkedinConnectRequest.class;
    }

    @Override
    protected LinkedinConnect createActionFromActionRequest(final LinkedinConnectRequest connectRequest) {
        return new LinkedinConnect(connectRequest.getRequesterRelevanteId(), connectRequest.getTargetProfileId());
    }

}
