package me.relevante.service.impl;

import me.relevante.api.TwitterApiClient;
import me.relevante.api.TwitterApiException;
import me.relevante.api.TwitterApiResponse;
import me.relevante.api.TwitterApiResult;
import me.relevante.service.NetworkBulkRequestService;
import me.relevante.core.RelevanteAccount;
import me.relevante.core.RelevanteContext;
import me.relevante.core.Twitter;
import me.relevante.core.TwitterPost;
import me.relevante.core.UniqueProfile;
import me.relevante.ingest.TwitterIngestPost;
import me.relevante.ingest.TwitterIngestProfile;
import me.relevante.request.TwitterSearchRelatedDataRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class TwitterSearchRelatedDataRequestService
        extends AbstractTwitterApiBulkRequestService<TwitterSearchRelatedDataRequest>
        implements NetworkBulkRequestService<Twitter, TwitterSearchRelatedDataRequest> {

    private static final Logger LOGGER = LoggerFactory.getLogger(TwitterSearchRelatedDataRequestService.class);

    private final TwitterIngestProcessor ingestProcessor;
    private final TwitterIngestConverter ingestConverter;
    private final TwitterLastPostAssigner lastPostAssigner;

    @Autowired
    public TwitterSearchRelatedDataRequestService(final CrudRepository<TwitterSearchRelatedDataRequest, String> actionRequestRepo,
                                                  final CrudRepository<RelevanteContext, String> relevanteContextRepo,
                                                  final CrudRepository<RelevanteAccount, String> relevanteAccountRepo,
                                                  final TwitterIngestProcessor ingestProcessor,
                                                  final TwitterIngestConverter ingestConverter,
                                                  final TwitterLastPostAssigner lastPostAssigner) {
        super(actionRequestRepo, relevanteContextRepo, relevanteAccountRepo);
        this.ingestProcessor = ingestProcessor;
        this.ingestConverter = ingestConverter;
        this.lastPostAssigner = lastPostAssigner;
    }

    @Override
    public Class<TwitterSearchRelatedDataRequest> getActionRequestClass() {
        return TwitterSearchRelatedDataRequest.class;
    }

    @Override
    protected TwitterApiResponse executeApiAction(final TwitterSearchRelatedDataRequest actionRequest,
                                                  final TwitterApiClient apiClient) {
        TwitterApiResponse twitterApiResponse;
        try {
            // We process posts before profiles so that when we index profiles for search, they have related posts
            final List<TwitterPost> searchRelatedPosts = apiClient.getSearchResults(actionRequest.getSearchTerms());
            final List<TwitterIngestPost> searchRelatedIngestPosts = convertToSearchRelatedIngestPosts(searchRelatedPosts);
            for (final TwitterIngestPost searchRelatedIngestPost : searchRelatedIngestPosts) {
                ingestProcessor.processIngestPost(searchRelatedIngestPost);
            }
            final List<TwitterIngestProfile> searchRelatedIngestProfiles = convertToSearchRelatedIngestProfiles(searchRelatedPosts);
            for (final TwitterIngestProfile searchRelatedIngestProfile : searchRelatedIngestProfiles) {
                final UniqueProfile uniqueProfile = ingestProcessor.processIngestProfile(searchRelatedIngestProfile);
                lastPostAssigner.assignLastPost(uniqueProfile.getProfileIdByNetwork(getNetwork()), actionRequest.getRequesterRelevanteId());
            }
            actionRequest.setExtractedProfiles(searchRelatedIngestProfiles);
            actionRequest.setExtractedPosts(searchRelatedIngestPosts);
            twitterApiResponse = new TwitterApiResponse(TwitterApiResult.SUCCESS, searchRelatedPosts);
        } catch (final TwitterApiException e) {
            LOGGER.error("Error", e);
            actionRequest.setError();
            twitterApiResponse = new TwitterApiResponse(TwitterApiResult.EXCEPTION, e);
        }
        return twitterApiResponse;
    }

    private List<TwitterIngestProfile> convertToSearchRelatedIngestProfiles(final List<TwitterPost> searchRelatedPosts) {
        final Map<Long, TwitterIngestProfile> ingestProfileById = new HashMap<>();
        for (final TwitterPost searchRelatedPost : searchRelatedPosts) {
            final TwitterIngestProfile ingestProfile = ingestConverter.createIngestProfileFromProfile(searchRelatedPost.getAuthor());
            ingestProfileById.put(ingestProfile.getId(), ingestProfile);
        }
        return new ArrayList<>(ingestProfileById.values());
    }

    private List<TwitterIngestPost> convertToSearchRelatedIngestPosts(final List<TwitterPost> searchRelatedPosts) {
        final Map<String, TwitterIngestPost> ingestPostById = new HashMap<>();
        for (final TwitterPost searchRelatedPost : searchRelatedPosts) {
            final TwitterIngestPost ingestPost = ingestConverter.createIngestPostFromPost(searchRelatedPost);
            ingestPostById.put(ingestPost.getId(), ingestPost);
        }
        return new ArrayList<>(ingestPostById.values());
    }

}
