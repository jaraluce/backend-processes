package me.relevante.service.impl;

import me.relevante.service.NetworkLastPostAssigner;
import me.relevante.core.Twitter;
import me.relevante.core.TwitterFullPost;
import me.relevante.core.TwitterFullProfile;
import me.relevante.persistence.NetworkFullPostRepo;
import me.relevante.request.TwitterEngageStatus;
import me.relevante.service.RelevanteProfileContextService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Component;

@Component
public class TwitterLastPostAssigner
        extends AbstractLastPostAssigner<Twitter, TwitterFullPost, TwitterFullProfile, TwitterEngageStatus>
        implements NetworkLastPostAssigner<Twitter> {

    @Autowired
    public TwitterLastPostAssigner(final CrudRepository<TwitterFullProfile, String> fullProfileRepo,
                                   final NetworkFullPostRepo<TwitterFullPost> fullPostRepo,
                                   final RelevanteProfileContextService relevanteProfileContextService) {
        super(fullProfileRepo, fullPostRepo, relevanteProfileContextService);
    }

    @Override
    public Twitter getNetwork() {
        return Twitter.getInstance();
    }

    @Override
    protected TwitterEngageStatus createNetworkEngageStatus() {
        return new TwitterEngageStatus();
    }

}
