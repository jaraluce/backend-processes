
package me.relevante;

import me.relevante.backend.Consumer;
import me.relevante.backend.queue.QueueName;
import me.relevante.queue.RemoveProfileFromIndexMessage;
import me.relevante.service.ProfileIndexService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class RemoveProfileFromIndexConsumer implements Consumer<RemoveProfileFromIndexMessage> {

    private static final Logger logger = LoggerFactory.getLogger(RemoveProfileFromIndexConsumer.class);

    private final ProfileIndexService profileIndexService;

    @Autowired
    public RemoveProfileFromIndexConsumer(final ProfileIndexService profileIndexService) {
        this.profileIndexService = profileIndexService;
    }

    @RabbitListener(queues = QueueName.REMOVE_PROFILE_FROM_INDEX)
	public void process(final RemoveProfileFromIndexMessage request) {
        try {
            profileIndexService.process(request);
        } catch (Exception e) {
            logger.error("Error processing Twitter bulk action", e);
        }
    }

}