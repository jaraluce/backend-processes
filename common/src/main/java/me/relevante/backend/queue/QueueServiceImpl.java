package me.relevante.backend.queue;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import me.relevante.queue.BulkActionMessage;
import me.relevante.queue.DeduplicationMessage;
import me.relevante.queue.EnrichmentMessage;
import me.relevante.queue.ProfileIndexMessage;
import me.relevante.queue.QueueService;
import me.relevante.queue.RemoveProfileFromIndexMessage;
import me.relevante.queue.SendEmailMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
public class QueueServiceImpl implements QueueService {

    private static final Logger LOGGER = LoggerFactory.getLogger(QueueServiceImpl.class);

    private final ConnectionFactory connectionFactory;
	private final ObjectMapper jsonMapper;

	@Autowired
	public QueueServiceImpl(final ConnectionFactory connectionFactory,
							final ObjectMapper jsonMapper) {
		this.connectionFactory = connectionFactory;
		this.jsonMapper = jsonMapper;
	}

	@Override
    public void sendSendEmailMessage(final SendEmailMessage message) {
		sendMessage(message, QueueName.SEND_EMAIL);
	}

	@Override
	public void sendDeduplicationMessage(final DeduplicationMessage message) {
		sendMessage(message, QueueName.DEDUPLICATION);
	}

	@Override
	public void sendProfileIndexMessage(final ProfileIndexMessage message) {
		sendMessage(message, QueueName.PROFILE_INDEX);
	}

	@Override
	public void sendRemoveProfileFromIndexMessage(final RemoveProfileFromIndexMessage message) {
		sendMessage(message, QueueName.REMOVE_PROFILE_FROM_INDEX);
	}

	@Override
	public void sendBulkActionMessage(final BulkActionMessage message) {
		sendMessage(message, QueueName.BULK_ACTIONS);
	}

	@Override
	public void sendEnrichmentMessage(final EnrichmentMessage message) {
		sendMessage(message, QueueName.ENRICHMENT);
	}

	private void sendMessage(final Object object,
                             final String... queuesNames) {
		final String message = serialize(object);
        for (final String queueName : queuesNames) {
            marshallMessage(queueName, message);
        }
    }

	private String serialize(final Object object) {
		try {
			return jsonMapper.writeValueAsString(object);
		} catch (final JsonProcessingException e) {
			LOGGER.error("Error", e);
			throw new IllegalArgumentException("Object couldn't be serialized", e);
		}
	}

	private boolean marshallMessage(final String queueName,
									final String message) {
		Connection connection = null;
		Channel channel = null;
		try {
			connection = connectionFactory.newConnection();
			channel = connection.createChannel();
			channel.queueDeclare(queueName, true, false, false, null);
			channel.basicPublish("", queueName, null, message.getBytes());
		}
		catch (Exception ex){
			LOGGER.error("Error", ex);
			return false;
		}
		finally{
			try {
				if (channel != null) channel.close();
				if (connection != null) connection.close();
			}
			catch (IOException ex) {
				LOGGER.error("Error", ex);
			}
		}
		return true;
	}

}