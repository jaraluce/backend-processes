package me.relevante.backend.persistence;

import me.relevante.persistence.NetworkActionRequestRepo;
import me.relevante.request.TwitterReplyRequest;
import org.springframework.stereotype.Repository;

@Repository
public interface TwitterReplyRequestRepo extends NetworkActionRequestRepo<TwitterReplyRequest> {
}
