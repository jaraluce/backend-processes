package me.relevante.service.impl;

import me.relevante.service.NetworkBulkRequestService;
import me.relevante.core.Linkedin;
import me.relevante.core.LinkedinFullProfile;
import me.relevante.core.LinkedinProfile;
import me.relevante.core.RelevanteContext;
import me.relevante.core.RelevanteProfileContext;
import me.relevante.core.UniqueProfile;
import me.relevante.core.UpdateStrategy;
import me.relevante.integration.LinkedinProfileIntegrator;
import me.relevante.request.LinkedinExtractContactsRequest;
import me.relevante.request.LinkedinExtractedContact;
import me.relevante.service.RelevanteProfileContextService;
import me.relevante.util.LinkedinUrlComposer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class LinkedinExtractContactsRequestService
        extends AbstractNetworkBulkRequestService<Linkedin, LinkedinExtractContactsRequest>
        implements NetworkBulkRequestService<Linkedin, LinkedinExtractContactsRequest> {

    private final LinkedinProfileIntegrator profileIntegrator;
    private final RelevanteProfileContextService relevanteProfileContextService;

    @Autowired
    public LinkedinExtractContactsRequestService(final CrudRepository<LinkedinExtractContactsRequest, String> actionRequestRepo,
                                                 final CrudRepository<RelevanteContext, String> relevanteContextRepo,
                                                 final LinkedinProfileIntegrator profileIntegrator,
                                                 final RelevanteProfileContextService relevanteProfileContextService) {
        super(actionRequestRepo, relevanteContextRepo);
        this.profileIntegrator = profileIntegrator;
        this.relevanteProfileContextService = relevanteProfileContextService;
    }

    @Override
    public Linkedin getNetwork() {
        return Linkedin.getInstance();
    }

    @Override
    public Class<LinkedinExtractContactsRequest> getActionRequestClass() {
        return LinkedinExtractContactsRequest.class;
    }

    @Override
    protected void execute(final LinkedinExtractContactsRequest extractContactsRequest) {
        if (extractContactsRequest.isNotSuccessful()) {
            return;
        }
        final List<LinkedinExtractedContact> extractedContacts = extractContactsRequest.getExtractedContacts();
        for (final LinkedinExtractedContact extractedContact : extractedContacts) {
            final LinkedinFullProfile fullProfile = createFullProfileFromExtractedContact(extractedContact);
            final UniqueProfile uniqueProfile = profileIntegrator.integrate(fullProfile, UpdateStrategy.UPDATE_WITH_EXISTING_PROVIDED);
            setUniqueProfileAsContact(uniqueProfile.getId(), extractContactsRequest.getRequesterRelevanteId());
        }
    }

    private LinkedinFullProfile createFullProfileFromExtractedContact(final LinkedinExtractedContact extractedContact) {
        final LinkedinProfile profile = createProfileFromExtractedContact(extractedContact);
        return new LinkedinFullProfile(profile);
    }

    private LinkedinProfile createProfileFromExtractedContact(final LinkedinExtractedContact extractedContact) {
        final LinkedinProfile profile = new LinkedinProfile();
        profile.setId(extractedContact.getPublicIdentifier());
        profile.setProfileUrl(LinkedinUrlComposer.toPublicProfileUrlfromPublicIdentifier(extractedContact.getPublicIdentifier()));
        profile.setFirstName(extractedContact.getFirstName());
        profile.setLastName(extractedContact.getLastName());
        profile.setName(extractedContact.getFirstName() + " " + extractedContact.getLastName());
        profile.setHeadline(extractedContact.getOccupation());
        return profile;
    }

    private void setUniqueProfileAsContact(final String uniqueProfileId,
                                           final String relevanteId) {
        final RelevanteProfileContext relevanteProfileContext = relevanteProfileContextService.getOrCreate(relevanteId, uniqueProfileId);
        relevanteProfileContext.setContact(true);
        relevanteProfileContextService.save(relevanteProfileContext);
    }
}
