package me.relevante.backend.persistence;

import me.relevante.request.LinkedinExtractContactsRequest;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LinkedinExtractContactsRequestRepo extends MongoRepository<LinkedinExtractContactsRequest, String> {
}
