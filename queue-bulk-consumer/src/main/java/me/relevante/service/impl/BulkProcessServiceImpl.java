package me.relevante.service.impl;

import me.relevante.service.BulkProcessService;
import me.relevante.service.NetworkBulkRequestService;
import me.relevante.queue.BulkActionMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Daniel Ibanez
 */
@Service
public class BulkProcessServiceImpl implements BulkProcessService {

    private final Map<String, NetworkBulkRequestService> networkBulkProcessServiceByActionClass;

    @Autowired
    public BulkProcessServiceImpl(final Collection<NetworkBulkRequestService> networkBulkProcessServices) {
        this.networkBulkProcessServiceByActionClass = new HashMap<>();
        networkBulkProcessServices.forEach(service -> networkBulkProcessServiceByActionClass.put(service.getActionRequestClass().getSimpleName(), service));
    }

    public BulkActionMessage process(final BulkActionMessage bulkActionRequest) {
        final NetworkBulkRequestService networkBulkProcessService = networkBulkProcessServiceByActionClass.get(bulkActionRequest.getActionClassName());
        return networkBulkProcessService.process(bulkActionRequest);
    }
}
